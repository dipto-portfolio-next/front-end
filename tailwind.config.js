/* eslint-disable global-require */
/* eslint-disable unicorn/prefer-module */

module.exports = {
  purge: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}',
  ],
  darkMode: false,
  theme: {
    fontFamily: {
      inter: ['Inter', 'sans-serif'],
      poppins: ['Poppins', 'sans-serif'],
    },
    extend: {
      transitionTimingFunction: {
        'in-drawer-open': 'cubic-bezier(0.820, 0.085, 0.395, 0.895)',
      },
      colors: {
        // Configure your color palette here
      },
      backgroundImage: () => ({
        header: "url('/images/vectors/header_bg.svg')",
        about: "url('/images/vectors/about_bd.svg')",
        contact: "url('/images/vectors/contact_bg.svg')",
      }),
    },
  },
  variants: {
    extend: {},
  },
  plugins: [require('daisyui')],
  daisyui: {
    themes: [
      {
        mytheme: {
          primary: '#36B37E',
          'primary-focus': '#008552',
          'primary-content': '#ffffff',
          secondary: '#f000b8',
          'secondary-focus': '#bd0091',
          'secondary-content': '#ffffff',
          accent: '#37cdbe',
          'accent-focus': '#2aa79b',
          'accent-content': '#ffffff',
          neutral: '#344563',
          'neutral-focus': '#2a2e37',
          'neutral-content': '#ffffff',
          'base-100': '#ffffff',
          'base-200': '#f9fafb',
          'base-300': '#d1d5db',
          'base-content': '#172B4D',
          info: '#2094f3',
          success: '#009485',
          warning: '#ff9900',
          error: '#ff5724',
        },
      },
    ],
  },
};
