import {
  faFreeCodeCamp,
  faGithub,
  faLinkedin,
  faMedium,
  faTwitter,
} from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { v4 as uuidv4 } from 'uuid';

import { info } from '../data/info';

const Footer = (): JSX.Element => {
  const {
    socialLinks: {
      github, linkedin, twitter, freecodecamp, medium,
    },
  } = info;

  const socialLinks = [
    { link: github, icon: faGithub },
    { link: linkedin, icon: faLinkedin },
    { link: twitter, icon: faTwitter },
    { link: freecodecamp, icon: faFreeCodeCamp },
    { link: medium, icon: faMedium },
  ];
  return (
    <section className="border-t border-gray-300 border-solid">
      <div className="flex font-semibold justify-center items-center mx-auto text-3xl py-6 lg:w-1/6">
        {socialLinks.map(({ link, icon }) => (
          <a
            key={uuidv4()}
            href={link}
            className="w-5 m-2"
            target="_blank"
            rel="noopener noreferrer"
          >
            <FontAwesomeIcon
              className="text-gray-700 hover:text-gray-900"
              icon={icon}
            />
          </a>
        ))}
      </div>
    </section>
  );
};

export default Footer;
