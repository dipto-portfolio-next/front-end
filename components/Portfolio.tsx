/* eslint-disable security/detect-object-injection */
import { useEffect, useState } from 'react';
import { v4 as uuidv4 } from 'uuid';

import { info } from '../data/info';
import Card from './Card';
import Modal from './Modal';

const Portfolio = ():JSX.Element => {
  const { projects } = info;
  const [index, setIndex] = useState('');
  const [data, setData] = useState({});
  const [open, setOpen] = useState(false);

  const handleIndexSave = (id) => {
    setIndex(id);
  };

  useEffect(() => {
    const selectedId = Number.parseInt(index, 10);

    if (selectedId >= 0) {
      setData(projects[selectedId]);
      setOpen(true);
    } else {
      setOpen(false);
      setData({});
    }
  }, [index, projects]);

  return (
    <>
      <section className="min-h-screen portfolio">
        <div className="container mx-auto pt-20 lg:pt-0 px-16">
          <h1 className="font-bold font-inter text-2xl md:text-3xl text-base-content py-4 text-center">
            My Recent Works
          </h1>
          <div className="my-5 grid grid-cols-1 gap-4 md:grid-cols-2 lg:grid-cols-3">
            {projects.map(
              ({ title, feature, techs }, projectIndex) => (
                <Card
                  key={uuidv4()}
                  id={projectIndex}
                  title={title}
                  feature={feature}
                  skills={techs}
                  handleIndexSave={handleIndexSave}
                />
              ),
            )}
          </div>
        </div>
      </section>
      {open && (
        <Modal data={data} handleIndexSave={handleIndexSave} />
      )}
    </>
  );
};

export default Portfolio;
