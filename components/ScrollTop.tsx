import { faChevronUp } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { animateScroll as scroll } from 'react-scroll';

const handleClick = () => {
  scroll.scrollToTop({
    duration: 1500,
    delay: 100,
    smooth: true,
  });
};

const ScrollTop = (): JSX.Element => (
  <button
    className="fixed bottom-0 right-0 mr-5 mb-5 focus:outline-none border-none bg-green-500 p-2 rounded-lg bg-opacity-25 hover:bg-opacity-75"
    type="button"
    onClick={handleClick}
  >
    <FontAwesomeIcon className="w-7 text-white" icon={faChevronUp} />
  </button>
);

export default ScrollTop;
