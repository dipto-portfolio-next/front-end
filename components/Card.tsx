/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { v4 as uuidv4 } from 'uuid';

import { stringUtils } from '../utils/common';

const Card = ({
  title,
  feature,
  skills,
  id,
  handleIndexSave,
}): JSX.Element => {
  const handleClick = () => {
    // eslint-disable-next-line no-console
    handleIndexSave(id);
  };
  return (
    <div className="card bordered text-center shadow-xl">
      <figure>
        <img src={feature} alt="project_cover" />
      </figure>
      <div className="card-body">
        <h2 className="card-title font-poppins text-base md:text-lg xl:text-xl text-base-400">
          {title}
        </h2>
        <div className="flex flex-wrap items-center justify-center">
          {skills
            .sort(stringUtils.sortByLengthAndCharAsc)
            .map((item) => (
              <div
                key={uuidv4()}
                className="badge badge-ghost bg-gray-200 badge-sm p-2 rounded-md mx-1 font-inter font-semibold text-base-content text-xs xl:text-sm"
              >
                {item}
              </div>
            ))}
        </div>
        <div className="justify-center card-actions">
          <button
            type="button"
            className="btn btn-primary font-inter text-xs md:text-sm xl:text-base my-4"
            onClick={handleClick}
          >
            See Project
          </button>
        </div>
      </div>
    </div>
  );
};

export default Card;
