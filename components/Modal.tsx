/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { faGithub } from '@fortawesome/free-brands-svg-icons';
import { faExternalLinkAlt, faTimes } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { v4 as uuidv4 } from 'uuid';

import ModalStyles from '../styles/Modal.module.css';
import { stringUtils } from '../utils/common';

const Modal = ({ data, handleIndexSave }): JSX.Element => {
  const {
    title, feature, repo, live, techs, description,
  } = data;

  const links = [
    { href: live, label: 'See live', icon: faExternalLinkAlt },
    { href: repo, label: 'See source', icon: faGithub },
  ];

  const handleClose = (event_) => {
    event_.stopPropagation();
    handleIndexSave('');
  };

  return (
    <div className="fixed top-0 left-0 w-full h-full z-50 bg-gray-600 bg-opacity-25 backdrop-filter backdrop-blur-md">
      <div
        className={`${ModalStyles.center} bg-white w-11/12 md:w-4/5 xl:w-3/5 rounded-lg p-3 relative`}
      >
        <div className="absolute top-8 right-5 md:relative md:top-0 md:right-2">
          <FontAwesomeIcon
            className="w-7 text-base-100 md:text-gray-600 bg-gray-200 bg-opacity-25 md:bg-opacity-100 py-1 px-2 rounded ml-auto cursor-pointer"
            icon={faTimes}
            onClick={handleClose}
          />
        </div>
        <figure>
          <img
            className="my-2 rounded-lg"
            src={feature}
            alt="feature img"
          />
        </figure>
        <div className="w-full flex flex-col md:flex-row md:flex-wrap items-center my-4">
          <h3 className="text-xl md:text-2xl font-bold text-base-content">
            {title}
          </h3>
          <div className="flex mt-3 items-center justify-center md:ml-auto md:mt-0 order-last md:order-none">
            {links.map(({ href, label, icon }) => (
              <a
                key={uuidv4()}
                className="flex items-center btn btn-primary font-bold font-inter mr-2 text-xs md:text-base"
                href={href}
                target="_blank"
                rel="noreferrer"
              >
                {label}
                <FontAwesomeIcon className="w-4 ml-2" icon={icon} />
              </a>
            ))}
          </div>
          <div className="flex flex-wrap items-center mt-2 justify-center md:justify-start md:w-full">
            {techs
              .sort(stringUtils.sortByLengthAndCharAsc)
              .map((item) => (
                <div
                  key={uuidv4()}
                  className="badge badge-ghost bg-gray-200 badge-sm p-2 rounded-md mx-1 font-inter font-semibold text-base-content text-center text-xs md:text-sm"
                >
                  {item}
                </div>
              ))}
          </div>
          <p className="text-sm font-normal font-inter text-justify text-gray-500 mt-4 max-h-28 overflow-y-auto md:text-lg">
            {description}
          </p>
        </div>
      </div>
    </div>
  );
};

export default Modal;
