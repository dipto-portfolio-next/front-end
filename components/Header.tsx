import {
  faFreeCodeCamp, faGithub, faLinkedin, faMedium, faTwitter,
} from '@fortawesome/free-brands-svg-icons';
import { faChevronDown } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link as ScrollLink } from 'react-scroll';
import { v4 as uuidv4 } from 'uuid';

import { info } from '../data/info';
import Nav from './Nav';

const Header = (): JSX.Element => {
  const {
    intro,
    title,
    description,
    socialLinks: {
      github, linkedin, twitter, freecodecamp, medium,
    },
  } = info;

  const socialLinks = [
    { link: github, icon: faGithub },
    { link: linkedin, icon: faLinkedin },
    { link: twitter, icon: faTwitter },
    { link: freecodecamp, icon: faFreeCodeCamp },
    { link: medium, icon: faMedium },
  ];

  return (
    <div className="min-h-screen bg-header bg-no-repeat xl:bg-cover lg:bg-contain bg-center">
      <div className="container mx-auto relative">
        <Nav />
        <div className="hero py-20 px-5  md:p-32 lg:p-10">
          <div className="hero-content text-center">
            <div className="md:max-w-xl">
              <h1 className="mb-2 text-2xl xl:mb-5 md:text-3xl xl:text-5xl font-bold flex flex-col">
                <span className="text-gray-800 md:py-1 xl:py-2">
                  {intro}
                </span>
                <span className="text-primary md:py-1 xl:py-2">
                  {title}
                </span>
              </h1>
              <p className="font-normal text-lg xl:text-xl text-neutral leading-8 mx-auto py-2 ">
                {description}
              </p>
              <div className="flex  font-semibold justify-center items-center py-3">
                {socialLinks.map(({ link, icon }) => (
                  <a
                    key={uuidv4()}
                    href={link}
                    className="w-7 mx-2"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <FontAwesomeIcon
                      className="text-gray-700 hover:text-gray-900"
                      icon={icon}
                    />
                  </a>
                ))}
              </div>
              <ScrollLink
                to="portfolio"
                smooth="true"
                className="border border-solid border-gray-200 rounded-full w-10 h-10 flex items-center justify-center mx-auto my-8 cursor-pointer"
              >
                <FontAwesomeIcon
                  icon={faChevronDown}
                  className="text-gray-400 w-3.5"
                />
              </ScrollLink>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Header;
