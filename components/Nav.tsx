/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import { faBars, faTimes } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useState } from 'react';
import { Link } from 'react-scroll';
import { v4 as uuidv4 } from 'uuid';

import { menus } from '../data/ui';

const Nav = (): JSX.Element => {
  const [open, setOpen] = useState(false);
  const toggleDrawer = () => {
    setOpen(!open);
  };
  return (
    <nav className="h-full">
      <div className="hidden lg:flex lg:justify-center lg:py-6">
        {menus.map(({ path, name }) => (
          <Link
            key={uuidv4()}
            to={path}
            smooth
            className="btn btn-link font-bold lg:text-xs xl:text-base text-base-content hover:text-primary cursor-pointer hover:no-underline"
          >
            {name}
          </Link>
        ))}
      </div>
      <div
        className={`fixed top-0 left-0 p-4 w-full bg-base-100 shadow-xl ${
          open ? 'h-full' : ''
        } z-50 lg:hidden`}
      >
        {!open && (
          <button
            type="button"
            className="btn btn-ghost"
            onClick={toggleDrawer}
          >
            <FontAwesomeIcon className="w-7" icon={faBars} />
          </button>
        )}
        {open && (
          <ul className="menu">
            <button
              type="button"
              className="btn btn-ghost self-end"
              onClick={toggleDrawer}
            >
              <FontAwesomeIcon className="w-4" icon={faTimes} />
            </button>
            {menus.map(({ path, name }) => (
              <li key={uuidv4()}>
                <Link
                  to={path}
                  smooth
                  className="btn btn-link font-bold text-base text-base-content hover:text-primary cursor-pointer hover:no-underline"
                >
                  <span onClick={toggleDrawer}>{name}</span>
                </Link>
              </li>
            ))}
          </ul>
        )}
      </div>
    </nav>
  );
};

export default Nav;
