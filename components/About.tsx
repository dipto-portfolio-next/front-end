import { v4 as uuidv4 } from 'uuid';

import { info } from '../data/info';
import { stringUtils } from '../utils/common';

const About = (): JSX.Element => {
  const { skillsObject, aboutMe, resume } = info;

  return (
    <section className="my-10 about pt-20 lg:pt-0" id="section-about">
      <section className="hero font-inter bg-about bg-no-repeat bg-cover">
        <section className="text-center hero-content flex flex-col">
          <div className="max-w-screen-xl">
            <h1 className="mb-3 font-bold text-4xl text-base-content">
              About me
            </h1>
            <p className="font-normal text-xl leading-8 text-gray-700 md:w-3/4 mx-auto p-2">
              {aboutMe}
            </p>
            <a
              className="btn btn-primary"
              href={resume}
              target="_blank"
              rel="noopener noreferrer"
            >
              Get My Resume
            </a>
          </div>
        </section>
      </section>
      <section className="container my-5 mx-auto grid grid-cols-1 gap-4 p-5 md:grid-cols-2 xl:grid-cols-3">
        {skillsObject.map(({
          title, values, imgSrc, imgAlt,
        }) => (
          <section
            key={uuidv4()}
            className="card shadow bg-gray-200 font-inter p-5 md:p-8 flex flex-col items-center"
          >
            <div className="w-16 h-16 md:w-20 md:h-20 bg-white rounded-full flex items-center justify-center shadow-2xl">
              <img src={imgSrc} alt={imgAlt} />
            </div>
            <div className="card-body">
              <h4 className="card-title font-semibold text-lg md:text-2xl leading-8 text-center text-gray-700 my-1 md:my-4">
                {title}
              </h4>
              <div className="flex flex-wrap justify-center my-2 md:my-5">
                {values
                  .sort(stringUtils.sortByLengthAndCharAsc)
                  .map((lang) => (
                    <div
                      key={uuidv4()}
                      className="badge badge-ghost p-1 md:p-2.5  bg-white font-semibold text-green-500 text-xs tracking-wide rounded-md m-1 text-center"
                    >
                      {lang}
                    </div>
                  ))}
              </div>
            </div>
          </section>
        ))}
      </section>
    </section>
  );
};

export default About;
