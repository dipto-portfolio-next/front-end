import emailjs from 'emailjs-com';
import { useFormik } from 'formik';
import { useEffect } from 'react';
import { toast } from 'react-toastify';
import * as Yup from 'yup';

const validationSchema = Yup.object({
  firstName: Yup.string().required('Please provide first name'),
  lastName: Yup.string().required('Please provide last name'),
  email: Yup.string()
    .email('Invalid email address')
    .required('Please your email'),
  message: Yup.string().required('Please provide your message'),
});

const ContactForm = (): JSX.Element => {
  const formik = useFormik({
    initialValues: {
      firstName: '',
      lastName: '',
      email: '',
      message: '',
    },
    onSubmit: async (values, { resetForm }) => {
      try {
        const {
          firstName, lastName, email, message,
        } = values;
        const templateParameters = {
          from_name: `${firstName} ${lastName}`,
          from_email: email,
          message,
        };
        const response = await emailjs.send(
          process.env.NEXT_PUBLIC_SERVICE_ID,
          process.env.NEXT_PUBLIC_TEMPLATE_ID,
          templateParameters,
        );
        if (response.status === 200) {
          toast.success('👍 Email sent!', {
            position: 'bottom-right',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          resetForm();
        }
      } catch {
        toast.error('🔥 Failed to send email! Please send email to diptokmk47@gmail.com', {
          position: 'bottom-right',
          autoClose: 10_000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    },
    validationSchema,
  });

  useEffect(() => {
    emailjs.init(process.env.NEXT_PUBLIC_USER_ID);
  }, []);

  return (
    <form
      className="flex flex-col flex-1 p-5"
      onSubmit={formik.handleSubmit}
    >
      <div className="grid grid-cols-1 lg:grid-cols-2 gap-3 mb-3">
        <div className="form-control">
          <input
            name="firstName"
            className="input input-primary input-lg input-bordered outline-none"
            id="firstName"
            type="text"
            placeholder="First Name"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.firstName}
          />
          {formik.touched.firstName && formik.errors.firstName ? (
            <p className="mt-2 text-red-500 text-sm">
              {formik.errors.firstName}
            </p>
          ) : undefined}
        </div>
        <div className="form-control">
          <input
            name="lastName"
            className="input input-primary input-lg input-bordered outline-none"
            id="lastName"
            type="text"
            placeholder="Last Name"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.lastName}
          />
          {formik.touched.lastName && formik.errors.lastName ? (
            <p className="mt-2 text-red-500 text-sm">
              {formik.errors.lastName}
            </p>
          ) : undefined}
        </div>
      </div>
      <div className="form-control mb-3">
        <input
          name="email"
          className="input input-primary input-lg input-bordered outline-none"
          id="email"
          type="email"
          placeholder="Email"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.email}
        />
        {formik.touched.email && formik.errors.email ? (
          <p className="mt-2 text-red-500 text-sm">
            {formik.errors.email}
          </p>
        ) : undefined}
      </div>
      <div className="form-control mb-5">
        <textarea
          className="textarea h-40 textarea-bordered text-lg textarea-primary"
          name="message"
          id="message"
          placeholder="Type your message"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.message}
        />
        {formik.touched.message && formik.errors.message ? (
          <p className="mt-2 text-red-500 text-sm">
            {formik.errors.message}
          </p>
        ) : undefined}
      </div>
      <button
        className="btn btn-primary btn-lg lg:w-2/5 lg:text-base"
        type="submit"
      >
        Get in touch
      </button>
    </form>
  );
};

export default ContactForm;
