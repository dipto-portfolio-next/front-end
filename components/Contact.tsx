import ContactForm from './ContactForm';

const Contact = (): JSX.Element => (
  <section className="contact min-h-screen mt-10 pt-20 lg:pt-0 lg:py-20 bg-contact bg-no-repeat bg-center bg-cover lg:bg-contain">
    <section
      className="container flex flex-col lg:flex-row mx-auto"
      id="section-contact"
    >
      <p className="lg:py-14 p-5 m-1 font-bold text-xl md:text-3xl lg:max-w-md xl:max-w-2xl">
        I&apos;m always interested in hearing about new projects, so
        if you&apos;d like to chat please get in touch.
      </p>
      <ContactForm />
    </section>
  </section>
);

export default Contact;
