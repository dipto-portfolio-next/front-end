import Head from 'next/head';

const Layout = ({ children }) => (
  <div className="bg-white overflow-hidden font-inter">
    <Head>
      <title>Dipto Karmakar</title>
      <link rel="icon" href="/favicon.ico" />
      <meta
        name="description"
        content="Dipto karmakar's portfolio site. Full-stack web developer, software engineer, python developer, rails developer, ruby on rails developer, freelancing, fulltime job, django"
      />
    </Head>
    <main>{children}</main>
  </div>
);

export default Layout;
