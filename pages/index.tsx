import About from '../components/About';
import Contact from '../components/Contact';
import Footer from '../components/Footer';
import Header from '../components/Header';
import Portfolio from '../components/Portfolio';
import ScrollTop from '../components/ScrollTop';

export default function Main(): JSX.Element {
  return (
    <>
      <Header />
      <Portfolio />
      <About />
      <Contact />
      <Footer />
      <ScrollTop />
    </>
  );
}
