/* eslint-disable import/prefer-default-export */
export const info = {
  intro: 'Hey there. I’m Dipto.',
  title: 'I’m a software developer',
  description:
    'I can help you build a product , feature or website. Look through some of my work and experience! If you like what you see and have a project you need coded,don’t hesitate to contact me.',
  aboutMe:
    "Programming is Dipto's craft. That means he values giving and receiving mentorship, and cares about diversity, inclusiveness, and empathy in Tech. He is a devoted software engineer with 4+ years of experience and proficiency in Ruby, Python, and JavaScript programming languages. Dipto enjoys contributing to open-source projects, blogging about programming, organizing tech events, and enthusiastically exploring new challenges.  A capable collaborator, Dipto loves working with companies to define their vision and applying his technical expertise to bring that vision to life.",
  resume:
    'https://drive.google.com/file/d/1tByzqkmGtNSS0ogC0P0ioLJQiL7QAx_V/view?usp=sharing',
  socialLinks: {
    github: 'https://github.com/dipto0321',
    linkedin: 'https://www.linkedin.com/in/diptokarmakar/',
    freecodecamp:
      'https://www.freecodecamp.org/news/author/dipto0321/',
    medium: 'https://medium.com/@imdipto',
    angellist: '',
    twitter: 'https://twitter.com/imdiptokmk',
  },
  skillsObject: [
    {
      title: 'Languages',
      values: ['Python', 'Ruby', 'Javascript', 'HTML', 'CSS'],
      imgSrc: '/images/vectors/langIcon.svg',
      imgAlt: 'language icon',
    },
    {
      title: 'Frameworks/Libraries',
      values: [
        'Django',
        'Django REST framework',
        'Flask',
        'Ruby on Rails',
        'Express.js',
        'React',
        'Next.js',
        'Redux',
        'Jest',
        'Axios',
        'PyTest',
        'RSpec',
        'Bootstrap',
        'TailwindCSS',
        'Styled',
        'Material-UI',
        'Numpy',
        'Panda',
      ],
      imgSrc: '/images/vectors/frameIcon.svg',
      imgAlt: 'framework icon',
    },
    {
      title: 'Skills/Tools',
      values: [
        'TDD',
        'Git',
        'GitHub',
        'GitLab',
        'GitHib flow',
        'Git flow',
        'Terminal',
        'Webpack',
        'Heroku',
        'Netlify',
        'Teamwork',
        'Jupyter',
        'DigitalOcean',
        'Google Cloud',
        'Chrome/Firefox Dev tool',
        'Remote Pair-Programming',
      ],
      imgSrc: '/images/vectors/skillIcon.svg',
      imgAlt: 'skill icon',
    },
  ],

  projects: [
    {
      title: 'My portfolio',
      repo: 'https://gitlab.com/dipto-portfolio-next/front-end',
      live: 'https://dipto-portfolio-next.netlify.app/',
      feature: '/images/projects/portfolio.png',
      techs: ['Next.js', 'TailwindCss', 'Daisyui'],
      description:
        'My portfolio site. This site is fully responsive. Deployed in netlify',
    },
    {
      title: 'Video browser',
      repo: 'https://gitlab.com/dipto0321/video-browser',
      live: 'https://video-browser-react.netlify.app/',
      feature: '/images/projects/video_browser.png',
      techs: ['React', 'Google API', 'HTML5'],
      description: 'A video browser',
    },
  ],
};
