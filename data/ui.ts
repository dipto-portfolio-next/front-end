/* eslint-disable import/prefer-default-export */
export const menus = [
  { path: 'portfolio', name: 'Portfolio' },
  { path: 'about', name: 'About' },
  { path: 'contact', name: 'Contact' },
];
