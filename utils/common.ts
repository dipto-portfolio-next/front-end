/* eslint-disable import/prefer-default-export */
export const stringUtils = {
  sortByLengthAndCharAsc: (a: string, b: string): number => (
    a.length - b.length || a.localeCompare(b)
  ),
};
